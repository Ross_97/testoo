﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] int playerHealth = 5;
    [SerializeField] Text healthText;
    [SerializeField] AudioClip takeDamage;
    // Start is called before the first frame update
    void Start()
    {
        healthText.text = playerHealth.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        GetComponent<AudioSource>().PlayOneShot(takeDamage);
        playerHealth--;
        healthText.text = playerHealth.ToString();
        if (playerHealth <= 0)
        {
            print("youDead");
        }
        
    }
}
