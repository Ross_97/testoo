﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour
{
    [SerializeField] Tower tower;
    [SerializeField] int towerLimit = 5;
    [SerializeField] Transform towerParentTransform;
    

    Queue<Tower> towerQueue = new Queue<Tower>();

    public void AddTower(Block baseBlock)
    {
        int numberOfTowersPlaced = towerQueue.Count;
        if (numberOfTowersPlaced < towerLimit)
        {
            InstanciateNewTower(baseBlock);
        }
        else
        {
            MoveTower(baseBlock);
        }
    }

    private void MoveTower(Block newBaseBlock)
    {
        var oldTower = towerQueue.Dequeue();

        oldTower.baseBlock.isPlacable = true;
        newBaseBlock.isPlacable = false;

        oldTower.baseBlock = newBaseBlock;

        oldTower.transform.position = newBaseBlock.transform.position;

        towerQueue.Enqueue(oldTower);
    }

    private void InstanciateNewTower(Block baseBlock)
    {
        var newTower = Instantiate(tower, baseBlock.transform.position, Quaternion.identity);
        baseBlock.isPlacable = false;

        newTower.baseBlock = baseBlock;
        baseBlock.isPlacable = false;

        towerQueue.Enqueue(newTower);
    }
}
