﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] int Hits = 3;
    [SerializeField] ParticleSystem hitParticle;
    [SerializeField] ParticleSystem deathParticle;
    [SerializeField] Transform parent;
    [SerializeField] AudioClip enemyTakeDamage;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnParticleCollision(GameObject other)
    {
        hitParticle.Play();
        GetComponent<AudioSource>().PlayOneShot(enemyTakeDamage);
        Hits--;
        if (Hits <= 0)
        {
            KillEnemy();
        }
    }

    private void KillEnemy()
    {
        var VFX = Instantiate(deathParticle, transform.position, Quaternion.identity);
        VFX.Play();
        float destroyDelay = VFX.main.duration;
        Destroy(VFX.gameObject, destroyDelay);

        Destroy(gameObject);

        
    }

    
}
