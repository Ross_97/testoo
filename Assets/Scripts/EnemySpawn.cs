﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawn : MonoBehaviour
{
    [Range(0.1f, 100f)]
    [SerializeField] float secondsBetweenSpawn = 3f;
    [SerializeField] EnemyMovement enemy;
    [SerializeField] Transform enemyParentTransform;
    [SerializeField] Text enemySpawnAmount;
    [SerializeField] AudioClip enemySpawnAudio;

    int numberOfEnemiesSpawned = 0;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(RepeatidleySpawnEnemy());
        enemySpawnAmount.text = numberOfEnemiesSpawned.ToString();
    }

  
    IEnumerator RepeatidleySpawnEnemy()
    {
        while (true)
        {
            var newEnemy = Instantiate(enemy, transform.position, Quaternion.identity);
            newEnemy.transform.parent = enemyParentTransform;
            yield return new WaitForSeconds(secondsBetweenSpawn);
            numberOfEnemiesSpawned++;
            enemySpawnAmount.text = numberOfEnemiesSpawned.ToString();
            GetComponent<AudioSource>().PlayOneShot(enemySpawnAudio);
        }
       

    }
}
