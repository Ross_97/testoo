﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] List<Block> path;
    [SerializeField] float enemyMovement = 0.5f;
    [SerializeField] ParticleSystem victoryParticle;
    // Start is called before the first frame update
    void Start()
    {
       
        PathFinder pathfinder = FindObjectOfType<PathFinder>();
        var path = pathfinder.GetPath();
        StartCoroutine(FollowPath(path));
    }

    IEnumerator FollowPath(List<Block>path)
    {
        
        foreach (Block waypoint in path)
        {
            transform.position = waypoint.transform.position;
            yield return new WaitForSeconds(enemyMovement);
        }
        var VFX = Instantiate(victoryParticle, transform.position, Quaternion.identity);
        VFX.Play();
        float destroyDelay = VFX.main.duration;
        Destroy(VFX.gameObject, destroyDelay);

        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
