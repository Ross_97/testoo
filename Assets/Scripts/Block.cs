﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public bool isExplored = false;
    public Block exploredFrom;
    public bool isPlacable = true;
   

    const int gridSize = 10;
    Vector2 gridPos;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public int GetGridSize()

    {
        return gridSize;
    }

    public Vector2Int GetGridPos()
    {
        return new Vector2Int(
        Mathf.RoundToInt(transform.position.x / gridSize), 
        Mathf.RoundToInt(transform.position.z / gridSize) 
        );
    }
   public void SetTopColour(Color color)
    {
        MeshRenderer topMeshRenderer = transform.Find("Top").GetComponent<MeshRenderer>();
        topMeshRenderer.material.color = color;
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (isPlacable)
            {
                FindObjectOfType<TowerFactory>().AddTower(this);
            }
            else
            {
                print("Can't Place Here");
            }
            
        }
        else
        {

        }

    }
}
