﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[SelectionBase]
[RequireComponent (typeof(Block))]
public class EditorSnap : MonoBehaviour
{
   
    Block block;


    void Awake()
    {
        block = GetComponent<Block>();
    }

    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        SnapToGrid();
        UpdateLabel();

    }

    private void UpdateLabel()
    {
        TextMesh textMesh = GetComponentInChildren<TextMesh>();
            string labelText = 
            block.GetGridPos().x + 
            "," + 
            block.GetGridPos().y;
        textMesh.text = labelText;
        gameObject.name = labelText;
    }

    private void SnapToGrid()
    {
        int gridSize = block.GetGridSize();
        transform.position = new Vector3(
            block.GetGridPos().x * gridSize, 
            0f, 
            block.GetGridPos().y * gridSize
            );
    }
}
