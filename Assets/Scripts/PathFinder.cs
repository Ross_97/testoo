﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour
{

    [SerializeField] Block startBlock, endBlock;


    Dictionary<Vector2Int, Block> grid = new Dictionary<Vector2Int, Block>();
    Queue<Block> queue = new Queue<Block>();

    bool isRunning = true;

    Block searchCentre;
    List<Block> path = new List<Block>();

    Vector2Int[] directions =
    {
        Vector2Int.up,
        Vector2Int.right,
        Vector2Int.down,
        Vector2Int.left
    };

    public List <Block>GetPath()
    {
        if (path.Count == 0)
        {
            CalculatePath();
        }

        return path;
    }

    private void CalculatePath()
    {
        LoadBlocks();
        ColourStartAndEnd();
        BreadthFirstSearch();
        CreatePath();
    }

    // Start is called before the first frame update
    void Start()
    {
        
        //ExploreNeighbours(Block searchCentre);
    }

    private void CreatePath()
    {
        SetAsPath(endBlock);

        Block previous = endBlock.exploredFrom;
        while (previous != startBlock)
        {
            SetAsPath(previous);
            previous = previous.exploredFrom;
        }
        SetAsPath(startBlock);
        path.Reverse();
    }


    private void SetAsPath(Block block)
    {
        path.Add(block);
       block.isPlacable = false;
    }


    private void ExploreNeighbours()
    {
        if (!isRunning) { return; }

        foreach (Vector2Int direction in directions)
        {
            Vector2Int explorationCoordinates = searchCentre.GetGridPos() + direction;
            if (grid.ContainsKey(explorationCoordinates))
            {
                QueueNewNeighbours(explorationCoordinates);
            }
        }
    }

    private void QueueNewNeighbours(Vector2Int explorationCoordinates)
    {
        Block neighbour = grid[explorationCoordinates];
        if (neighbour.isExplored || queue.Contains(neighbour))
        {

        }
        else
        {
            queue.Enqueue(neighbour);
            neighbour.exploredFrom = searchCentre;
        }
    }

    private void HaltIfEndFound()
    {
        if (searchCentre == endBlock)
        {
            isRunning = false;
        }
    }

    private void ColourStartAndEnd()
    {
        startBlock.SetTopColour(Color.green);
        endBlock.SetTopColour(Color.red);
    }

    void LoadBlocks()
    {
        var blocks = FindObjectsOfType<Block>();
        foreach (Block block in blocks)
        {
            bool isOverlapping = grid.ContainsKey(block.GetGridPos());
            if (isOverlapping)
            {

            }
            else
            {
                grid.Add(block.GetGridPos(), block);
                block.SetTopColour(Color.black);
            }
            
        }
    }

    private void BreadthFirstSearch()
    {
        queue.Enqueue(startBlock);
        while (queue.Count > 0 && isRunning)
        {
            searchCentre = queue.Dequeue();
            HaltIfEndFound();
            ExploreNeighbours();
            searchCentre.isExplored = true;
        }
    }



// Update is called once per frame
void Update()
    {
        
    }
}
